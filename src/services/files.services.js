export async function uploadFile(data) {
    const response = await fetch('/api/upload-file', {
        method: 'POST',
        body: data
    });
    return await response.json();
}

export async function getFiles(data) {
    const response = await fetch(`/api/files?userKey=${data.key}&$limit=100`, {
        method: 'GET',
        headers: { "Content-Type": "application/json" }
    });
    return await response.json();
}

export async function deleteFile(file){
    const response = await fetch(`/api/files/${file.id}`, {
        method: 'DELETE',
    });
    return await response.json();
}
