export async function createUser(data) {
  const response = await fetch("/api/users", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(data)
  });
  return await response.json();
}

export async function getUser(data) {
  const response = await fetch(`/api/users/${data.key}`, {
    method: "GET",
    headers: { "Content-Type": "application/json" }
  });
  return await response.json();
}
