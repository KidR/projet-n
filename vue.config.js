// On redirige les flux /api du client (Vue) vers notre serveur REST (Feathers)
module.exports = {
  devServer: {
    proxy: {
      "^/api": {
        target: "http://localhost:3030",
        changeOrigin: true,
        pathRewrite: { "^/api": "" }
      }
    }
  }
};
