#!/bin/bash

echo "++ Dev setup initialization ++"
echo "> Install Sqlite3"
sudo apt install sqlite3
echo "> Setup database data.db"
sqlite3 data.db < sql/init_db.sql 
echo "> Database data.db is created"
