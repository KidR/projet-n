/* eslint-disable no-console */

// files-model.js - A KnexJS
// 
// See http://knexjs.org/
// for more of what you can do here.
const metaModel = require('./metamodel');

module.exports = function (app) {
  const db = app.get('knexClient');
  const tableName = metaModel.tables.files;
  db.schema.hasTable(tableName).then(exists => {
    if(!exists) {
      db.schema.createTable(tableName, table => {
        table.increments('id');
        table.string('randname');
        table.string('name');
        table.string('userKey', 32);
        table.foreign('userKey').references('user.key');
      })
        .then(() => console.log(`Created ${tableName} table`))
        .catch(e => console.error(`Error creating ${tableName} table`, e));
    }
  });
  

  return db;
};
