/* eslint-disable no-console */

// tags-model.js - A KnexJS
// 
// See http://knexjs.org/
// for more of what you can do here.
const metaModel = require('./metamodel');

module.exports = function (app) {
  const db = app.get('knexClient');
  const tableName = metaModel.tables.tags;
  db.schema.hasTable(tableName).then((exists) => {
    if (!exists) {
      db.schema.createTable(tableName, table => {
        table.string('name').primary();
      })
      .then(() => console.log(`Created ${tableName} table`))
      .catch(e => console.error(`Error creating ${tableName} table`, e));
    }
  });

  // Create qualification table
  const qualifTableName = 'qualification';
  db.schema.hasTable(qualifTableName).then((exists) => {
    if (!exists) {
      db.schema.createTable(qualifTableName, table => {
        table.integer('fileId').unsigned();
        table.string('tagName');

        table.foreign('fileId').references('file.id');
        table.foreign('tagName').references('tag.name');
      })
      .then(() => console.log('Created ' + qualifTableName + ' table'))
      .catch(e => console.error('Error creating ' + qualifTableName + ' table', e));
    }
  });

  return db;
};
