module.exports = {
    tables: {
        "users": "user" ,
        "files": "file",
        "tags": "tag"
    },
    path:{
        uploadRootPath:"../uploads" // Keep in mind that this path is from the folder api/
    }
};