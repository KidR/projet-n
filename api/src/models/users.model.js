/* eslint-disable no-console */

// users-model.js - A KnexJS
//
// See http://knexjs.org/
// for more of what you can do here.
const metaModel = require('./metamodel');

module.exports = function (app) {
  const db = app.get('knexClient');
  const tableName = metaModel.tables.users;
  db.schema.hasTable(tableName).then(exists => {
    if(!exists) {
      db.schema.createTable(tableName, table => {
        table.string('key').primary();
        table.string('email').unique().notNullable();
      })
        .then(() => console.log(`Created ${tableName} table`))
        .catch(e => console.error(`Error creating ${tableName} table`, e));
      // Add public user
      db('user').insert({
        key: 'public',
        email: 'null'
      })
      .then(() => console.log('Created public user'))
      .catch(e => console.error('Could not create public user', e));
    }
  });
  return db;
};
