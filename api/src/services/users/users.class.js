const { Service } = require('feathers-knex');

const crypto = require('crypto');
const KEY_BYTES = 10;

const metaModel = require('../../models/metamodel');

exports.Users = class Users extends Service {
  constructor(options) {
    super({
      ...options,
      name: metaModel.tables.users,
      id: "key"
    });
  }
  
  generateKey(bytes=KEY_BYTES){
    return crypto.randomBytes(bytes).toString('hex');
  }

  async create(data, params){
    // TODO: @Noa check if we use crypto correctly -> digest() or toString()??
    data.key = this.generateKey();
    return super.create(data,params);
  }
  
};
