const { Service } = require('feathers-knex');
const FormData = require('form-data');
const metaModel = require('../../models/metamodel');
const fs = require('fs');
const path = require('path');

exports.Files = class Files extends Service {
  constructor(options) {
    super({
      ...options,
      name: metaModel.tables.files
    });
  };

  async remove(id, params) {
    // Delete in DB
    const deleteInformation = await super.remove(id, params);

    // Delete on disk
    fs.unlinkSync(path.join(metaModel.path.uploadRootPath, deleteInformation.userKey, deleteInformation.randname));

    return deleteInformation;
  };
};
