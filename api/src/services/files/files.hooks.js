async function populateTags(context) {
  const addTags = async message => {
    let tags = await context.app.get('knexClient')('qualification')
    .where('fileId', message.id)
    .select('tagName');
    tags = tags.map(elt => elt.tagName);
    return {
      ...message,
      tags
    }
  };
  
  if (context.method === 'find') {
    context.result.data = await Promise.all(context.result.data.map(addTags));
  } else {
    context.result = await addTags(context.result);
  }
  return context;
}

async function deleteTags(context) {
  // Delete associated tags
  const db = context.app.get('knexClient');
  await db('qualification')
  .where('fileId', context.id)
  .del();
  return context;
}

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [populateTags],
    get: [populateTags],
    create: [],
    update: [],
    patch: [],
    remove: [deleteTags]
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
