const { Service } = require('feathers-knex');

const metaModel = require('../../models/metamodel');

exports.Tags = class Tags extends Service {
  constructor(options) {
    super({
      ...options,
      name: metaModel.tables.tags,
      id: 'name'
    });
  }
};
