// eslint-disable-next-line no-unused-vars
const metadata = require('../models/metamodel');
const multer = require('multer');
const upload = multer({ dest: metadata.path.uploadRootPath });
const fs = require('fs');
const path = require('path');


module.exports = function (app) {
  // Add your custom middleware here. Remember that
  // in Express, the order matters.

  app.get('/download-file/:userKey/:id', async function(req,res) {
    // Get the user key associated with the file
    const file = await app.service('files').get(req.params.id);
    res.download(path.join('../uploads/',req.params.userKey,file.randname), file.name);
  });

  app.post('/upload-file', upload.single('uploadedFile'), async (req, res) => {
    try {
      // The destination of the file is the folder under 'uploads/'
      let destPath = path.join(metadata.path.uploadRootPath,  req.body.key);
      if (!fs.existsSync(destPath)) {
        fs.mkdirSync(destPath);
      }

      const extension = req.file.originalname.split(".");
      if (extension.length > 1){
        req.file.filename = req.file.filename + '.' + extension[extension.length - 1];
      }

      // Move the file to the user folder
      fs.renameSync(req.file.path, path.join(destPath, req.file.filename));
      
      try{
        // Create file in the database
        const createValue = await app.service('files').create({
          randname: req.file.filename,
          name: req.file.originalname,
          userKey: req.body.key
        }, null);

        // Qualification of files with tags
        const db = app.get('knexClient');
        if (req.body.tags) {
          req.body.tags.split(',').forEach(tag => {
            db('qualification').insert({
              fileId: createValue.id,
              tagName: tag
            })
            .catch(e => console.error('Error', e));
          });
        }

        // Response to the client
        res.send({
          status: true,
          message: 'File successfuly uploaded',
          createValue});
      } catch (err){
        // An error occurs when inserting file or qualitifications in DB
        fs.unlinkSync(path.join(destPath, req.file.filename))
        res.status(500).send("Error inserting file in DB");
      }
    } catch (err) {
      // An error occurs manipulating the file, the file is still at the root folder
      fs.unlinkSync(req.file.path)
      res.status(500).send("Error manipulating locally the file");
    }
  });
};