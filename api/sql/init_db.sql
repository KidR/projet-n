CREATE TABLE user(
    key CHAR(32) PRIMARY KEY NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE
);

-- Rajouter un attribut date a la fin
CREATE TABLE file(
    id INT PRIMARY KEY NOT NULL,
    path VARCHAR(255) NOT NULL,
    parent VARCHAR(100),
    userKey CHAR(32) NOT NULL,
    FOREIGN KEY(userKey) REFERENCES user(key)
);

CREATE TABLE tag(
    name VARCHAR(30) PRIMARY KEY NOT NULL
);

CREATE TABLE qualification(
    tag VARCHAR(30) NOT NULL,
    fileId INT NOT NULL,
    FOREIGN KEY(tag) REFERENCES tag(name),
    FOREIGN KEY(fileId) REFERENCES file(id),
    PRIMARY KEY(tag,fileId)
);