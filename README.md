# Umi (海)

codename: projet-n

name: Umi (海)

## Installation

```sh
git clone https://gitlab.com/KidR/projet-n.git
cd projet-n/
```

### Setup front-end

```sh
npm install && npm run serve
```

### Setup back-end

```sh
cd api/
npm install && npm start
```

## Présentation

Umi (海) est une plate-forme de dépôt de fichiers centralisée sur le web. 

L'objectif de cette application est de fournir un moyen simple et élégant pour **stocker des fichiers en interne**. Elle permet la création rapide d'un dépôt protégé par une clé aléatoire.

Umi n'est **pas destiné** à être déployé sur Internet pour l'instant. Actuellement, le dépôt proposé est unique. Les utilisateurs peuvent demander une clé pour créer un sous-dépôt mais ne peuvent pas créer dépôt en tant qu'administrateur.

### Origine du projet

Ce projet a été la réponse au besoin suivant :

Un professeur souhaite posséder un moyen d'héberger des fichiers afin de récupérer les travaux de ses élèves tout au long de l'année. En effet, la plateforme Moodle ne correspond pas à ses besoins qui sont :

- Création rapide d'un sous-dépôt par élève
- Récupérer les fichiers simplement
- Récupérer tous les fichiers correspondant à une étiquette précise (ex: Tp1)
- Filtrer le contenu des fichiers par une étiquette
- Un dépôt commun pour lui et ses élèves

## Fonctionnalités 

### Implémentés

Date: 15/11/2020

-   Créer un sous-dépôt

-   Accéder à un sous-dépôt
-   Accéder au sous-dépôt public

-   Déposer un fichier dans un sous-depôt
-   Attribuer des étiquettes à un fichier
-   Parcourir les fichiers déposés
-   Supprimer les fichiers déposés
-   Télécharger un fichier déposé
-   Rechercher les fichiers par nom
-   Rechercher les fichiers par étiquette
-   Trier les fichiers par nom
-   Trier les fichiers par extension

### Futures

-   Partager un fichier déposé par un lien temporaire
-   Améliorer l'API par ligne de commande (protéger les accès - accès unique par l'administrateur)
-   Télécharger des fichiers par étiquette en 1 clique (une ligne de commande: `GET /api/files?tags=<tag>`)
-   Télécharger des fichiers par utilisateur en 1 clique (une ligne de commande: `GET /api/files?key=<user>`)
-   Généralisation de l'application => Gestionnaire d'instances de Umi (海) afin de permettre à tout utilisateur de créer un dépôt en tant qu'administrateur.

## Sécurité et choix

Cette application n'a pas pour but premier d'être sécurisée. En effet, elle se base sur une authentification faible afin de permettre la création d'un sous-dépôt rapidement et sans contraintes.

La clé est générée aléatoirement sur un nombre d'octets arbitraires (`N=10`). Il est possible de changer la génération de cette clé par un algorithme plus robuste tel que **sha256** (choix non retenu car la longueur de la clé trop longue).

Cette plateforme, en l'état, se veut être déployé de manière interne (Intranet) et non sur Internet.

Dans un futur, on peut imaginer virtualiser ce processus. La web app ne sera plus un dépôt mais **un gestionnaire/hébergeur de dépôt.** La web app permettra à un utilisateur de créer un dépôt et d'en être administrateur. Cet utilisateur pourra partager son dépôt à d'autres utilisateurs afin qu'ils puissent créer des **sous-dépôts** dans ce dernier.

## Glossaire

>   But

Héberger des fichiers dans un dépôt centralisé publique qui est partitionné en sous-dépôt protégé par une clé.

>   Dépôt

Un dépôt représente le serveur hébergeant la web app.

>   Sous-dépôt

Un sous-dépôt est protégé par une **clé** qui fait référence à un utilisateur. Créer un sous-dépôt permet de stocker des fichiers dont on souhaite restreindre les accès aux personnes disposant de la clé du dépôt.

>   Clé

Une clé représente l'unique moyen d'accéder à un sous-dépôt `d` d'un dépôt `D`. Il existe 2 types de clés : 

1.  Clé originale : obtenu à la création d'un sous-dépôt, elle permet l'intégralité des permissions. 

2.  Clé *hashé* (non-implémentée) : permet uniquement la lecture d'un sous-dépôt. Idée d'un *access token* qui est crée lorsqu'on souhaite partagé un sous-dépôt, ou une partie de celui-ci, en lecture .

>   Tag

Chaque fichier déposé peut se voir associer un **tag**. Ce tag permet un filtrage efficace, notamment pour l'administrateur du dépôt.